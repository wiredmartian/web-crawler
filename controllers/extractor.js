const spellchecker = require('spellchecker');

module.exports = {
    rx:  /(<script(\s|\S)*?<\/script>)|(<style(\s|\S)*?<\/style>)|(<!--(\s|\S)*?-->)|(<\/?(\s|\S)*?>)/g,
    html: null,
    extractText: function(){
        if (this.html != null && this.html != '')
            return this.html.text().replace(this.rx, '');
        return null;
    },
    removeWhiteSpace: function(){
        if(this.html != null && this.html != ''){
            let text = this.extractText();
            if(text != null){
                return text.replace(/\s+/g, ' ');
            } else {
                console.log('No text');
            }
        } else {
            console.log('No html');
        }
    },
    checkSpelling: function(){
        let cleanText = this.removeWhiteSpace();
        if(cleanText != null){
            let errs = spellchecker.checkSpelling(cleanText);
            if(errs.length > 0){
                return errs.length;
            } else {
                return 0;
            }
        }
    },
    outputSpellingErrors: function(){
        if(this.html != '' && this.checkSpelling() != 0){
            let text = this.removeWhiteSpace(),
                output = [];
                if(text != null && typeof text != undefined){
                    let errs = Array.from(spellchecker.checkSpelling(text));
                    errs.forEach(function(item, i){
                        let word = text.substring(errs[i].start, errs[i].end);
                        output.push(word);
                    });
                }
            return output.filter(this.getUniqueValues);
        }
        return null;
    },
    getUniqueValues: function(value, index, self){
        return self.indexOf(value) === index;
    },
    findAndReplace: function(){
        let textBlock = this.removeWhiteSpace(),
            searchArray = Array.from(this.outputSpellingErrors());
        searchArray.forEach(function(word, index){
            textBlock = textBlock.toString().replace(word, "<b>" + word + "</b>");
        });
        return textBlock;
    }
}
