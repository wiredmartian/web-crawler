var express = require('express');
var router = express.Router();
const extractor = require('../controllers/extractor.js');
const cheerio = require('cheerio');
const request = require('request');

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Crawler' });
});

router.post('/', function(req, res, next){
  
  let url = req.body.url,
      data = '',
      markedHtml = '',
      haserrors = undefined;
  request.get(url, function(error, response, body){
      if(!error){
        let $ = cheerio.load(body);
        extractor.html = $('html');
        if(extractor.html != '' && extractor.html.length > 0){
          data = extractor.outputSpellingErrors();
          markedHtml = extractor.findAndReplace();
        }
        if(data.length > 0)
          haserrors = true;
        res.render('index', { title: 'Crawler', words: data, count: data.length, haserr: haserrors, marker: markedHtml });
      }
  })

})

module.exports = router;
